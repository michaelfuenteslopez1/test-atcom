import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject, Subscriber} from 'rxjs';
import {UserInterface} from '../Interface/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public allUser: UserInterface[] = [];
  public emitter: any;
  public digit: any;
  public data: any;
  public counter: Observable<number>;
  public user: Observable<UserInterface>;
  public table: Observable<UserInterface[]>;

  constructor(public http: HttpClient) {
    this.user = new Observable<UserInterface>(e => this.emitter = e);
    this.counter = new Observable<number>(e => this.digit = e);
    this.table = new Observable<UserInterface[]>(e => this.data = e);
  }


  getUser = () => {
    const urlApi = 'https://jsonplaceholder.typicode.com/users';
    return this.http.get(urlApi);
  };

  getUsers = () => {

    return this.counter;

  };

  saveUsers = (users: UserInterface[]) => {
    this.allUser = [...users, ...this.allUser];
  };

  createUser(userNew: UserInterface) {
    this.allUser.push(userNew);
    this.digit.next(this.allUser.length);
    this.emitter.next(userNew);

  }

  getNewUser = () => {
    return this.user;
  };

  getQuantity = () => {

  };

  getTable = () => {
    return this.table;
  };

  deleteUser = (id: number) => {
    for (let i = 0; i < this.allUser.length; i++) {
      if (this.allUser[i].id === id) {
        this.allUser.splice(i, 1);
        break;
      }
    }
    this.digit.next(this.allUser.length);
    this.data.next(this.allUser);
  };
}
