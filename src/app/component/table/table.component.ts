import {Component, OnInit} from '@angular/core';
import {UserService} from '../../service/user.service';
import {UserInterface} from '../../Interface/user.interface';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  public userData: UserInterface[];
  public cantidad: any;

  constructor(public userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getNewUser().subscribe((data) => {
      this.userData.push(data);
    });
    this.getAllUser();
    this.getQuantity();

    this.userService.getTable().subscribe( data => {
      this.userData = data;
    });
  }

  getAllUser() {
    this.userService
      .getUser()
      .subscribe((userData: UserInterface[]) => {
        this.userData = userData;
        this.userService.saveUsers(this.userData);
      });
  }

  getQuantity = () => {


  };

  onDeleteUser = (id: number) => {
    console.log('elimando id :', id);
    // this.userData.splice(0, id);
    this.userService.deleteUser(id);

  };
}
