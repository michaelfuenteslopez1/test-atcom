import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../service/user.service';
import {UserInterface} from '../../Interface/user.interface';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  public users: object[];
  public cantidad: number;

  constructor(public userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getUser().subscribe((users: object[]) => {
      // @ts-ignore
      this.users = users;
      console.log('usuarios', this.users.length);
      this.cantidad = this.users.length;
    });
    this.userService.getUsers().subscribe(e => {
      this.cantidad = e;
    });
  }


  counter = () => {

  };
}
