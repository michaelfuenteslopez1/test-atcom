import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserInterface} from '../../Interface/user.interface';
import {UserService} from '../../service/user.service';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public userForm: FormGroup;
  submit = false;

  constructor(public userService: UserService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {


    this.userForm = new FormGroup({
      id: new FormControl(Math.random()),
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      website: new FormControl('', [Validators.required])
    });
  }

  saveUser = () => {
    if (this.userForm.valid) {
      const newUser: UserInterface = this.userForm.getRawValue();
      console.log('usuario a push :', newUser);
      this.userService.createUser(newUser);
      this.toastr.success('USER SAVE');
      this.userForm.reset();
    } else {
      console.log('error');
      this.toastr.error('ERROR');
      this.submit = true;
    }
  }

  get type() {
    return this.userForm.controls;
  }

}
